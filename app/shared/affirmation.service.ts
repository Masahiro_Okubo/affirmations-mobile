import {Injectable} from "@angular/core";


@Injectable()
export class AffirmationService {

    private aff = new Array<Affirmation>(
        {id: 1, content: "Ter Stegen", category: [{id:1, name:"仕事"}], created_at:"2017-06-02 01:52:56", count:2},
        {id: 2, content: "Ter2 Stegen", category: [{id:1, name:"家族"}], created_at:"2017-06-02 01:52:56", count:2},
        {id: 3, content: "Ter3 Stegen", category: [{id:1, name:"社会貢献"}], created_at:"2017-06-02 01:52:56", count:2},
        {id: 4, content: "Ter4 Stegen", category: [{id:1, name:"読書"}], created_at:"2017-06-02 01:52:56", count:2},
        {id: 5, content: "Ter5 Stegen", category: [{id:1, name:"仕事"}], created_at:"2017-06-02 01:52:56", count:2}
    );

    getIAff(): Affirmation[] {
        return this.aff;
    }

    getOneAff(id: number): Affirmation {
        return this.aff.filter(item => item.id === id)[0];
    }
}

export class Affirmation {
    constructor(public id?: number,
                public content?: string,
                public category?: Category[],
                public created_at?: string,
                public count?: number) {
    }
}

export class Category {
    constructor(public id?: number,
                public name?: string) {
    }
}