import { Component, OnInit } from "@angular/core";

import { Item } from "./item";
import { ItemService } from "./item.service";

import {AffirmationService, Affirmation} from "../shared/affirmation.service";

@Component({
    selector: "ns-items",
    moduleId: module.id,
    templateUrl: "./items.component.html",
})
export class ItemsComponent implements OnInit {
    items: Item[];

    affirmations: Affirmation[];
    // This pattern makes use of Angular’s dependency injection implementation to inject an instance of the ItemService service into this class. 
    // Angular knows about this service because it is included in your app’s main NgModule, defined in app.module.ts.
    constructor(private itemService: ItemService, private affirmationService: AffirmationService) { }

    ngOnInit(): void {
        this.affirmations = this.affirmationService.getIAff();
        // this.items = this.itemService.getItems();
    }
}