import {Component, OnInit} from "@angular/core";
import {AffirmationService, Affirmation} from "../../shared/affirmation.service";

@Component({
    selector: "ns-index",
    moduleId: module.id,
    templateUrl: "./index.component.html",
})
export class IndexComponent implements OnInit {

    affirmations: Affirmation[];

    constructor(private affirmationService: AffirmationService) {
    }


    ngOnInit(): void {
        this.affirmations = this.affirmationService.getIAff();
    }
}