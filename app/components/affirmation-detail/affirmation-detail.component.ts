import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import {AffirmationService, Affirmation} from "../../shared/affirmation.service";

@Component({
    selector: "ns-affirmation-detail",
    moduleId: module.id,
    templateUrl: "./affirmation-detail.component.html",
})
export class AffirmationDetailComponent implements OnInit {
    aff: Affirmation;

    constructor(
        private affirmationService: AffirmationService,
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {
        const id = +this.route.snapshot.params["id"];
        this.aff = this.affirmationService.getOneAff(id);
    }
}
