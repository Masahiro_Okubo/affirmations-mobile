# README #

This repository contains the Affirmations Mobile source codes.

## Documentation ##

https://docs.nativescript.org/

## How to run code? ##

tns run android

tns run ios

tns livesync android
